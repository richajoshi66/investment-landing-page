import React, { ReactFragment } from "react";
import Motherdaughter from "../assets/mother_doughter.png";
import AfricanGirl from "../assets/girl_in_blacknwhite.png";
import CoupleHand from "../assets/couple_holding_hand.png";
import Money from "../assets/Money.png";
import BlueCurve from "../assets/BlueCurve.png";
import GreyCurve from "../assets/GreyCurve.png";
import Fade from 'react-reveal/Fade';
import CountUp, { useCountUp } from 'react-countup';

function Page(params) {
    return(
        <>
            <section id="container_1">
                    <div className="inner_container max-width-div">
                        <div className="flex-div">
                            <div className="column_1">
                                <h1 className="heading_banner"><span>Livis</span> is investing in your life.</h1>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it</p>
                                <button className="pinkBtn">Get Started</button>
                                <button className="transparentBtn">Scroll More</button>
                            </div>
                            <div className="column_1Parent">
                                <Fade bottom>

                                </Fade>
                                <div className="NavyBlueGraph">
                                    <div><p>Lorem ipsum</p> <p>15.72%</p></div>
                                    <div><p>$<CountUp end={5753.289} /></p></div>
                                </div>
                                <Fade bottom>
                                    <div className="column_2 img-column">
                                        <div className="img-1">
                                            <img src={Motherdaughter}/>
                                        </div>
                                        <div className="img-1 smallImg">
                                            <img src={AfricanGirl}/>
                                        </div>
                                        <div className="img-1">
                                            <img src={CoupleHand}/>
                                        </div>
                                    </div>
                                </Fade>
                                <div className="EarnedToday">
                                    <div className="flexClass">
                                    <div><img src= {Money}/></div>
                                    <div className="flex-col">
                                        <p>Earned Today</p>
                                        <p>$<CountUp end={2303} /></p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Fade bottom>
                        <div className="BlueCurve" style={{backgroundImage: 'url('+BlueCurve+')'}}></div>
                    </Fade>
                    <Fade bottom>
                        <div className="GreyCurve" style={{backgroundImage: 'url('+GreyCurve+')'}}></div>
                    </Fade>
            </section>
        </>
    )
}
export default Page;